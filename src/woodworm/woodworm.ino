#include "DHT.h"

int switched = 0; // shall the heater be switched on?

/* Digital pin 13: Switch power supply
 * Digital pin 2 : Communication DHT22
 * Digital pin 3 : CO warning LED
 * Analog pin A0 : Reading MQ9
 */
#define DHTPIN 2
#define CO_WARN_PIN 3
#define MQ_PIN A0
#define SWITCH_PIN 13
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE); 


// MQ9 calibration value, needs to be measured
float R0 = 0.90;
int intMQ9 = 0;
float voltMQ9 = 0.0; 
float RS_gas = 0.0;
float ratio = 0.0;

// Temperature setpoint
float thres = 25.0;
float hysteresis = 1.0;

void setup() {
  Serial.begin(9600); //Baud rate for serial connection
  dht.begin(); //Initiate DHT22
  
  pinMode(SWITCH_PIN, OUTPUT);    // sets the digital pin 13 as output
  digitalWrite(SWITCH_PIN, LOW);

  pinMode(CO_WARN_PIN, OUTPUT);
  digitalWrite(CO_WARN_PIN, LOW);

}

void loop() {
  // wait for sensor response
  delay(3000);

  // read sensor
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  if( switched == 1 && temperature > thres ) {
    switched = 0;
    digitalWrite(SWITCH_PIN, LOW);
  }
  
  if( switched == 0 && temperature < (thres-hysteresis) ) {
    switched = 1;
    digitalWrite(SWITCH_PIN, HIGH);
  }

  // Read MQ9
  intMQ9 = analogRead(MQ_PIN); 

  // Convert into ppm
  float voltMQ9 = ((float)intMQ9 / 1024) * 5.0; 
  float RS_gas = (5.0 - voltMQ9) / voltMQ9; 
  float ratio = RS_gas / R0; // ratio = RS/R0;
  // 300ppm corresponds approx. to ratio of 1.5
  // important, the *lower* the ratio, the higher the concentration

  if(ratio < 1.5) {
    digitalWrite(CO_WARN_PIN, HIGH);
  } else {
    digitalWrite(CO_WARN_PIN, LOW);
  }

    // send via serial
  Serial.print(humidity); //die Dazugehörigen Werte anzeigen
  Serial.print("\t");
  Serial.print(temperature);
  Serial.print("\t");
  Serial.println(ratio);
}
