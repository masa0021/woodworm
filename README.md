# Readme - Woodworm #

## What is this repository for? ##

* Re-use of existing arduino HW
* Get rid of a woodworm
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Setup ##

### Software ###

* Linux
* Gnuplot and logserial for live plotting
* Arduino IDE

### Parts ###

* Big chamber that can be heated up to 60-120 degC
* Arduino Nano, Due, ...
* Electrical heating device
* Switchable power socket controlled via GPIO pins, e. g. EVL SI230-3
* Temperature sensor, e. g. DHT22
* Several resistors and LEDs

Optional (if you are worried about smoke, carbon monoxid and the such)
* Air sensor, e. g. MQ-9
* Audio amp and speaker if you want to hear a warning signal in case of smoke :)

### Schematics ###

![Overview](./figs/setup.png)

### Pictures ###

![Dry run setup](./figs/dry_run.jpg)

## Run and live-plot ##

* ToDo
