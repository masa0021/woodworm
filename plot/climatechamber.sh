#!/bin/sh

SERIAL="/dev/ttyACM0"
BAUD=9600
LOG="/var/tmp/climatechamber.log"

touch "${LOG}"

logserial -t "${SERIAL}" -s $BAUD | awk '{ print systime(),"\t",$1,"\t",$2; fflush(); }' > "${LOG}" &

sleep 5

gnuplot -p <<-EOF
plot "${LOG}" using 1:2 ls 1 title "Humidity [%]", "${LOG}" using 1:3 ls 2 title "Temperature [°C]"
pause 5
while (1) {
  replot
  pause 1      # waiting time in seconds
}
EOF
